import 'package:fitness_time/data/providers/fit_activities_provider_impl.dart';
import 'package:fitness_time/ui/screen/fitness_home/widgets/fitness_activity_card.dart';
import 'package:flutter/material.dart';

class ActivitiesColumn extends StatelessWidget {
  ActivitiesColumn({super.key, required this.userId});
  final int userId;
  final FitnessProviderImpl fitness = FitnessProviderImpl();

  @override
  Widget build(BuildContext context) {
    List<Map<String, String>> listaActividades = fitness.activitiesList;
    if (listaActividades.isNotEmpty) {
      return Column(
          children: listaActividades
              .map((item) => FitnessActivityCard(
                    fitnessActivity: item["activity"]!,
                    date: item["date"]!,
                    distance: item["distance"]!,
                  ))
              .toList());
    } else {
      return Container();
    }
  }
}
