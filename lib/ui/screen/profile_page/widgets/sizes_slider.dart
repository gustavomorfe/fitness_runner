import 'package:fitness_time/constants.dart';
import 'package:flutter/material.dart';

class SizesSlider extends StatefulWidget {
  const SizesSlider(
      {super.key,
      required this.legend,
      required this.measurementUnit,
      required this.sliderRange});

  final String legend;
  final String measurementUnit;
  final SliderRange sliderRange;

  @override
  State<SizesSlider> createState() => _SizesSliderState();
}

class _SizesSliderState extends State<SizesSlider> {
  late double _currentSliderValue;
  late SliderRange sliderRange;
  @override
  void initState() {
    super.initState();
    _currentSliderValue = 50.0;
    sliderRange = widget.sliderRange;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(horizontal: 48.0),
        padding: EdgeInsets.zero,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Text(widget.legend),
            ),
            Expanded(
              flex: 8,
              child: Container(
                padding: EdgeInsets.zero,
                child: Slider(
                  min: sliderRange.min,
                  max: sliderRange.max,
                  activeColor: kHeliotrope,
                  inactiveColor: kPersianPink,
                  value: _currentSliderValue,
                  onChanged: (double value) {
                    setState(() {
                      _currentSliderValue = value;
                    });
                  },
                ),
              ),
            ),
            SizedBox(
              width: 54,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      (_currentSliderValue).toInt().toString(),textAlign: TextAlign.end,
                    ),
                  ),
                  const SizedBox(width: 3.0,),
                  Text(
                    widget.measurementUnit, textAlign: TextAlign.right,

                  ),
                ],
              ),
            ),
          ],
        ));
  }
}

class SliderRange {
  final double min;
  final double max;

  SliderRange({
    required this.min,
    required this.max,
  });
}
