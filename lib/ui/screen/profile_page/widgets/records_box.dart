import 'package:fitness_time/constants.dart';
import 'package:flutter/material.dart';

class RecordsBox extends StatelessWidget {
  const RecordsBox(
      {super.key,
      required this.iconData,
      required this.recordName,
      required this.duration});

  final IconData iconData;
  final String recordName;
  final String duration;

  @override
  Widget build(BuildContext context) {
    return Card(
      color: kHeliotrope,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Icon(iconData),
            Text(
              recordName,
            ),
            Text(
              duration,
            ),
          ],
        ),
      ),
    );
  }
}
