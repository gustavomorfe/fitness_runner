import 'package:fitness_time/constants.dart';
import 'package:fitness_time/ui/screen/profile_page/widgets/records_box.dart';
import 'package:fitness_time/ui/screen/profile_page/widgets/sizes_slider.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});
  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: InkWell(
              onTap: () => Navigator.pop(context),
              child: const Icon(Icons.arrow_back_ios)),
          title: const Center(
            child: Padding(
              padding: EdgeInsets.only(right: 80.0),
              child: Text(
                kMyProfile,
              ),
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 8.0,
              ),
              Hero(
                tag: 'profileImage',
                child: Container(
                  width: 200,
                  height: 200,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage(kUserImage),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
              Text(
                "Antonia Font",
                style: theme.textTheme.headlineMedium,
              ),
              const SizedBox(
                height: 4.0,
              ),
              Text(
                "Since 20 April 2022",
                style: theme.textTheme.bodyMedium,
              ),
              const SizedBox(
                height: 32.0,
              ),
              // ignore: prefer_const_constructors
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  RecordsBox(
                    iconData: Icons.schedule_outlined,
                    recordName: "Time",
                    duration: "2h 45'",
                  ),
                  SizedBox(
                    width: 24.0,
                  ),
                  RecordsBox(
                    iconData: Icons.pin_drop,
                    recordName: "Km",
                    duration: "212,4",
                  ),
                  SizedBox(
                    width: 24.0,
                  ),
                  RecordsBox(
                    iconData: Icons.directions_run,
                    recordName: "Activities",
                    duration: "42",
                  ),
                ],
              ),
              const SizedBox(
                height: 24.0,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizesSlider(
                    legend: "Height",
                    measurementUnit: "cm",
                    sliderRange: SliderRange(min: 50.0, max: 250.0),
                  ),
                  SizesSlider(
                    legend: "Weight",
                    measurementUnit: "kg",
                    sliderRange: SliderRange(min: 40.0, max: 150.0),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
