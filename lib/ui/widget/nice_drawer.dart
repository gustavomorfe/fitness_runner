import 'package:flutter/material.dart';

class NiceDrawer extends StatelessWidget {
  const NiceDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    const url = "assets/images/mujercorriendo.webp";

    return Drawer(
      child: SingleChildScrollView(
        child: SizedBox(
          height: 400.0,
          width: 200.0,
          child: Column(
            children: [
              Image.asset(
                url,
                height: 230.0,
                width: double.infinity,
                fit: BoxFit.cover,
              ),
              const SizedBox(
                height: 24,
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.symmetric(
                    horizontal: 6.0,
                  ),
                  height: 100,
                  child: const Text("Item 1"),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.symmetric(
                    horizontal: 6.0,
                  ),
                  height: 100,
                  child: const Text("Item 2"),
                ),
              ),
              const SizedBox(
                height: 4,
              ),
              Expanded(
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.symmetric(
                    horizontal: 6.0,
                  ),
                  height: 100,
                  child: const Text("Item 3"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
