import 'package:flutter/material.dart';

class FitnessActivityCard extends StatelessWidget {
  const FitnessActivityCard({
    super.key,
    required this.fitnessActivity,
    required this.distance,
    required this.date,
  });

  final String fitnessActivity;
  final String date;
  final String distance;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      height: 64,
      margin: const EdgeInsets.symmetric(vertical: 4.0),
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black26, //New
            blurRadius: 2.0,
            spreadRadius: 0.5,
            offset: Offset(
              0.3,
              1.8,
            ),
          ),
        ],
      ),
      child: Container(
        color: Colors.white,
        child: Row(
          children: [
            const Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.only(left: 8.0, right: 4),
                child: Icon(Icons.run_circle_outlined, size: 32),
              ),
            ),
            Expanded(
              flex: 5,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    fitnessActivity,
                    style: theme.textTheme.headlineMedium,
                  ),
                  Text(
                    date,
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Row(
                children: [
                  Expanded(
                    flex: 5,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          distance,
                          textAlign: TextAlign.end,
                          style: theme.textTheme.headlineSmall,
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Km",
                          style: theme.textTheme.headlineSmall,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
