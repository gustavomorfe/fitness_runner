import 'dart:ui';

const Color kMindaro = Color(0xFFedf67d);
const Color kPersianPink = Color(0xFFF896D8);
const Color kHeliotrope = Color(0xFFCA7DF9);
const Color kMajoreleBlue = Color(0xFF724CF9);
const Color kUltraViolet = Color(0xFF564592);
const Color kWhite = Color(0xFFfff4fb);
const Color kVeryLightGrey = Color(0xFFF2F2F2);

const kUserImage = "https://randomuser.me/api/portraits/women/44.jpg";

//Home
const String kMoreDetails = "Más detalles";
const String kLastActivities = "Últimas actividades";
const String kMonthlyTarget = "Objetivo mensual";
const String kHomeFitness = "Fitness home";

//Profile
const String kMyProfile = "Mi perfil";
