# Flutter Runner
## Just another definitive app

Flutter Runner es una aplicación Flutter diseñada para el aprendizaje práctico en un curso de desarrollo con Dart y Flutter del CIFO de L'Hospitalet. Simula un pseudoperfil de una persona que practica fitness. Muestra una interfaz interactiva para mostrar rutinas de ejercicio, monitorear el progreso, indicar peso y altura.

- Scaffold
- Navigator
- Appbar
- Bottom Navigation Bar
- Drawer
- Images
- Layouts
- Cards
- Sliders
- Percent indicator
- ✨Hero Magic ✨

## Cómo Clonar e Iniciar la Aplicación
Para obtener y ejecutar la aplicación en tu entorno local, sigue estos pasos:
### Clonar el Repositorio:
```sh
git clone https://gitlab.com/gustavomorfe/fitness_runner
```
### Navegar al Directorio del Proyecto:
```sh
cd FitnessApp
```
### Instalar Dependencias:
```sh
flutter pub get
```
### Ejecutar la Aplicación:
```sh
flutter run
```
¡Explora y disfruta Flutter Runner en tu propio entorno de desarrollo! Esta aplicación proporciona una base sólida para aprender y practicar conceptos esenciales de Flutter. ¡A ejercitarse! 🏋️‍♀️🏃‍♂️

## Dependecias

Flutter Runner necesita [Flutter](https://flutter.dev/) >=3.1.5 <4.0.0 para compilarse
